import math
import sys
import getopt
import queue

###
# A script for totaling up all the inputs for a given set of outputs.
# NOTE: This script _does not_ handle decisions between multiple recipes with the same output
#       The first output of a recipe is always the primary output that this recipe will be used for,
#       and if multiple recipes are presented for the same primary output, only the first recipe in the list will be used.
#       i.e. Don't place both the PP1 and PP2 BSE recipes in the mappings, make the decision on which to use yourself
#
#       The script handles recipes with multiple outputs, but they should generally be placed last in the mappings,
#       because if you do 1 BE, 1 AL, 1 SI = 2 BER @AML, then that recipe will be considered the primary recipe for
#       _BE, AL, and SI_. Even if you put the normal AL and SI recipes later on.
#
#       This script ain't here to make judgements on what recipes are best, that's 100% on you.
#       I mean they're _so_ context dependent.
#
#       # TODO CGS make it so a user can input materials to designate as endpoints
#                 (currently endpoints are just items without an associated recipe for them)
#       # TODO CGS Document the mapping, input, and output file formats
###

materials_produced = {}
materials_required = {}

buildings_required = set()

processing_queue = queue.Queue()


def process_materials(mapping_file, initial_materials_required_file, output_file, initial_materials_produced_file=None):
    mappings = build_mappings(mapping_file)

    initial_materials_required = parse_initial_materials(initial_materials_required_file)
    for required_material, required_amount in initial_materials_required.items():
        processing_queue.put([required_amount, required_material])

    if initial_materials_produced_file is not None:
        initial_materials_produced = parse_initial_materials(initial_materials_produced_file)
        for produced_material, produced_amount in initial_materials_produced.items():
            materials_produced[produced_material] = produced_amount

    while not processing_queue.empty():
        current_material_entry = processing_queue.get()
        current_material_required = current_material_entry[1]
        current_amount_required = current_material_entry[0]

        if current_material_required in materials_produced \
                and materials_produced[current_material_required] > 0:
            amount_already_produced = materials_produced[current_material_required]
            # We already have enough of the material, don't make more
            if amount_already_produced >= current_amount_required:
                materials_produced[current_material_required] -= current_amount_required
                continue
            else:
                # We have some of the current material produced, but not enough,
                # subtract and still make more
                current_amount_required -= amount_already_produced
                materials_produced[current_material_required] = 0

        if current_material_required not in mappings:
            # There isn't a recipe for this, it must be a base material
            # Add it to required materials, and skip recipe calculations
            if current_material_required in materials_required:
                materials_required[current_material_required] += current_amount_required
            else:
                materials_required[current_material_required] = current_amount_required
            continue

        recipe = mappings[current_material_required]
        # print(recipe)
        inputs = recipe[0]
        building = recipe[1]
        outputs = recipe[2]

        current_output_amount = outputs[current_material_required]

        # buildings_required is a set, so we don't have to worry about duplicates
        buildings_required.add(building)

        recipe_multiplier = math.ceil(current_amount_required / current_output_amount)

        # Produce enough outputs by executing the specified recipe
        for output_material, output_amount in outputs.items():
            amount_produced = output_amount * recipe_multiplier
            if output_material in materials_produced:
                materials_produced[output_material] += amount_produced
            else:
                materials_produced[output_material] = amount_produced

        # Reduce our supply by what we needed, either initially or to fill another recipe.
        materials_produced[current_material_required] = \
            max(materials_produced[current_material_required] - current_amount_required,
                0)

        # Pay for our recipe in inputs
        for input_material, input_amount in inputs.items():
            processing_queue.put([input_amount * recipe_multiplier, input_material])

    print("Finished processing...")
    write_output(output_file)


def parse_material(material_string):
    stripped_material = material_string.strip()
    material_entry = stripped_material.split(' ')
    material_entry[0] = int(material_entry[0])
    return material_entry


def parse_initial_materials(initial_materials_file):
    initial_materials = {}

    for line in initial_materials_file.readlines():
        if line.strip() == '':
            continue

        initial_materials_raw = line.split(',')

        for material in initial_materials_raw:
            parsed_material = parse_material(material)
            key = parsed_material[1]
            amount = int(parsed_material[0])

            if key in initial_materials:
                initial_materials[key] += amount
            else:
                initial_materials[key] = amount

    return initial_materials


def build_mappings(mapping_file):
    mappings = {}

    for line in mapping_file.readlines():
        if line.strip() == '':
            continue

        equation_sides = line.split('=')
        output_materials_raw = equation_sides[0].strip().split(',')
        output_materials_processed = {}

        # Identify any new primary keys the outputs of this recipe contain
        # In 95% of cases, this is just a single output with a new primary key
        new_primary_keys = []
        for material in output_materials_raw:
            parsed_material = parse_material(material)
            output_materials_processed[parsed_material[1]] = parsed_material[0]

            if parsed_material[1] not in mappings:
                new_primary_keys.append(parsed_material[1])

        right_values = equation_sides[1].split('@')

        input_materials_raw = right_values[0].split(',')
        input_materials_processed = {}

        for material in input_materials_raw:
            parsed_material = parse_material(material)
            input_materials_processed[parsed_material[1]] = parsed_material[0]

        for key in new_primary_keys:
            # [{input_name, amount...}, building_name, {output_name, amount...}]
            mappings[key] = [input_materials_processed, right_values[1].strip(), output_materials_processed]
    print("Finished mapping...")
    return mappings


def write_output(output_file):
    output_file.write("Resources Required:\n")
    required_resources = ""
    for material, amount in sorted(materials_required.items()):
        required_resources += material + " " + str(amount) + ", "
    required_resources = required_resources[:len(required_resources) - 2]
    output_file.write(required_resources + "\n")

    output_file.write("Buildings Required:\n")
    required_buildings_string = ""
    for building in sorted(buildings_required):
        required_buildings_string += building + ", "
    required_buildings_string = required_buildings_string[:len(required_buildings_string) - 2]
    output_file.write(required_buildings_string + "\n")

    output_file.write("Excess Resources Produced:\n")
    excess_resources = ""
    for material, amount in sorted(materials_produced.items()):
        if amount == 0:
            continue
        excess_resources += material + " " + str(amount) + ", "
    excess_resources = excess_resources[:len(excess_resources) - 2]
    output_file.write(excess_resources + "\n")
    print("Finished writing to output file...")


def get_help_string():
    return "Usage: material_totaling.py -r <required_materials_filepath> -m <mapping_filepath> -o <output_filepath> [-h] [-p already_produced_materials_filepath]"


def main(argv):
    try:
        opts, args = getopt.getopt(argv, 'r:m:o:hp:')
    except getopt.GetoptError:
        print(get_help_string())
        sys.exit(2)
    initial_materials_required_filepath = None
    mapping_filepath = None
    output_filepath = None
    initial_materials_produced_filepath = None
    for opt, arg in opts:
        if opt == '-h':
            print(get_help_string())
            sys.exit()
        elif opt == '-r':
            print("Initial requirements path: "+arg)
            initial_materials_required_filepath = arg
        elif opt == '-m':
            print("Initial mapping path: "+arg)
            mapping_filepath = arg
        elif opt == '-o':
            print("Initial output path: "+arg)
            output_filepath = arg
        elif opt == '-p':
            initial_materials_produced_filepath = arg

    # Spacer to separate argument output from process output
    print()

    if initial_materials_required_filepath is None\
            or mapping_filepath is None \
            or output_filepath is None:
        print("Missing arguments, " + get_help_string())
        sys.exit(2)

    initial_materials_required_file = None
    try:
        initial_materials_required_file = open(initial_materials_required_filepath, 'r')
    except FileNotFoundError:
        print("Input file does not exist " + initial_materials_required_filepath)
        sys.exit(2)

    mapping_file = None
    try:
        mapping_file = open(mapping_filepath, 'r')
    except FileNotFoundError:
        print("Mapping file does not exist " + mapping_filepath)
        sys.exit(2)

    output_file = open(output_filepath, 'w')

    initial_materials_produced_file = None
    if initial_materials_produced_filepath is not None:
        try:
            initial_materials_produced_file = open(initial_materials_produced_filepath, 'r')
        except FileNotFoundError:
            print("Input file does not exist " + initial_materials_produced_filepath)
            sys.exit(2)

    process_materials(mapping_file, initial_materials_required_file, output_file, initial_materials_produced_file)
    mapping_file.close()
    initial_materials_required_file.close()
    output_file.close()


if __name__ == '__main__':
    main(sys.argv[1:])


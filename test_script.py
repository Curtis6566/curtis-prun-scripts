import os

# TODO CGS For now just a way to run the script without pulling up the command line, in the future, actual test cases?

if __name__ == '__main__':
    os.system("material_totaling.py -r ./materials_required/COGC_including_lfabs.txt -m ./mappings/COGC_mapping.txt -o ./results/COGC_results_with_lfabs.txt")
    os.system("material_totaling.py -r ./materials_required/COGC_materials.txt -m ./mappings/COGC_mapping.txt -o ./results/COGC_results_no_lfabs.txt")
    os.system("material_totaling.py -r ./materials_required/Wildlife_park_build.txt -m ./mappings/Wildlife_park_mapping.txt -o ./results/Wildlife_park_build.txt")
    os.system("material_totaling.py -r ./materials_required/Wildlife_park_upkeep_per_30_days.txt -m ./mappings/Wildlife_park_mapping.txt -o ./results/Wildlife_park_upkeep_per_30_days.txt")
    #os.system("material_totaling.py -r ./materials_required/test_materials.txt -m ./mappings/test_mapping.txt -o ./results/produced_test.txt")
